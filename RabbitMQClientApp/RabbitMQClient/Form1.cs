using RabbitMQ.Client;
using System.Configuration;
using System.Text;

namespace RabbitMQClient
{
    public partial class Form1 : Form
    {
        IConnection _rabbitMQConnection;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            //var connection = ConfigurationManager.ConnectionStrings["RabbitMQConnectionString"].ConnectionString;
            var connectionFactory = new ConnectionFactory();
            connectionFactory.AutomaticRecoveryEnabled = true;
            connectionFactory.DispatchConsumersAsync = true;
            connectionFactory.UserName = "demo";
            connectionFactory.Password = "demo";
            connectionFactory.HostName = "localhost";
            connectionFactory.Port = 5672;
            connectionFactory.VirtualHost = "ClientAppVH";
            //connectionFactory.Uri = new Uri(connection);
            _rabbitMQConnection = connectionFactory.CreateConnection("RabbitMQAppConnection");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var chanel = _rabbitMQConnection.CreateModel())
            {
                chanel.ExchangeDeclare("Notification", ExchangeType.Direct, true, false);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var chanel = _rabbitMQConnection.CreateModel())
            {
                chanel.QueueDeclare("Email", true, false, false);
                chanel.QueueDeclare("SMS", true, false, false);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var changel = _rabbitMQConnection.CreateModel())
            {
                changel.QueueBind("Email", "Notification", "email");
                changel.QueueBind("SMS", "Notification", "sms");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (var chanel = _rabbitMQConnection.CreateModel())
            {
                var smsContent = txtSMS.Text;
                var properties = chanel.CreateBasicProperties();
                properties.DeliveryMode = 2;

                chanel.BasicPublish("Notification", "sms", properties, Encoding.UTF8.GetBytes(smsContent));
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var chanel = _rabbitMQConnection.CreateModel())
            {
                var emailContent = txtEmail.Text;
                var properties = chanel.CreateBasicProperties();
                properties.DeliveryMode = 2;    // 1 = not persistant, 2 = persistant;

                chanel.BasicPublish("Notification", "email", properties, Encoding.UTF8.GetBytes(emailContent));
            }
        }
    }
}